% q1.m
% A thermal simulation of an electric motor
% Created by Travis Llado, travisllado@utexas.edu, 2018-12-10

clear all

% User variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Vin = 24;           % input voltage, V, from datasheet
Rm0 = 0.316;        % motor reference resistance, ohms, from datasheet
rm = 317*2*pi/60;   % motor speed/voltage constant, rad/sec / V
aCu = 0.004041;     % temp.coef. for copper

Bl = 0;       % load friction
Bb = 1e-6;      % bearing friction
Bm = Bb + Bl;   % total mechanical friction
J = 0.001;      % motor moment of inertia

Cr = 0.1;       % motor thermal capacitance
ac = 0.0525;    % motor/ambient conduction scalar
Ta = 25;        % �C, from datasheet

duty1 = 0.1;
duty2 = 0.75;
freq = 1000;

dt = 0.00001;   % Timestep Length
tF = 3;         % Simulation Length

% Program variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t = 0:dt:tF;    % Simulation Time
x = zeros(2,length(t)); % [h T]
x(2,1) = Ta;
tau = zeros(1,length(t));

oneWave = [ones(1,1/freq/dt*duty1) zeros(1,1/freq/dt*(1-duty1))];
u = Vin*repmat(oneWave,1,tF*freq/2+1);

oneWave = [ones(1,1/freq/dt*duty2) zeros(1,1/freq/dt*(1-duty2))];
u = [u Vin*repmat(oneWave,1,tF*freq/2)];
    
% Simulation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bar = waitbar(0, 'Running Simulation');
for ii = 2:length(t)
    if mod(ii,round(length(t)/100)) == 0
        waitbar(ii/length(t),bar);
    end
    
    wm = x(1,ii-1)/J;
    Tr = x(2,ii-1);
    Rm = Rm0*(1+aCu*(Tr-Ta));
    Im = 1/Rm*Vin;
    fe = Im^2*Rm/Tr;
    fb = wm*Bb/Tr;
    fc = ac*(Tr-Ta);
    
    Ax = [  (-Bm/J-1/rm^2/Rm/J)*x(1,ii-1)   ;
            (fe+fb-fc)/Cr                   ];

    Bu = [  1/rm/Rm*u(ii)   ;
            0               ];
            
    dx = Ax + Bu;
    x(:,ii) = x(:,ii-1) + dx*dt;
    
    tau(ii) = 1/rm/Rm*Vin-1/rm^2/Rm/J*x(1,ii);
end

% Plot results %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
waitbar(1,bar,'Building plots');
figure
    subplot(3,1,1);
        plot(t,x(1,:)/J*60/2/pi, 'LineWidth',2);
        xlabel('t');
        ylabel('\omega (RPM)');
    subplot(3,1,2);
        plot(t,tau, 'LineWidth',2);
        xlabel('t');
        ylabel('\tau (Nm)');
    subplot(3,1,3);
        plot(t, x(2,:), 'LineWidth',2);
        xlabel('t');
        ylabel('T_{rotor} (�C)');
close(bar);

disp('Steady State Operating Point:')
disp(['speed (RPM) =     ' num2str(x(1,end)/J*60/2/pi)])
disp(['torque (Nm) =     ' num2str(tau(end))])
disp(['temperature (C) = ' num2str(x(2,end))])

% End of file %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%