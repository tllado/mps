% Mass-Spring-Damper Simulation

% User variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
b = 10;
k = 10;
m = 10;
g = -9.81;

A = [   0   -1/m    ;
        k   -b/m    ];
B = [ 	1   0   ;
        b   m   ];

v = @(x) sin(10*x);

dt = 0.001;
tF = 20;

% Program variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t = 0:dt:tF;
x = zeros(length(A), length(t));
u = [   v(t)                    ;
        repmat(g, 1,length(t))  ];

% Simulation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for ii = 2:length(t)
    dx = A*x(:,ii-1) + B*u(:,ii);
    x(:,ii) = x(:,ii-1) + dx*dt;
end

% Plot results %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
    subplot(1,2,1)
        plot(t,x(1,:));
        xlabel('t');
        ylabel('x');
    subplot(1,2,2)
        plot(t,x(2,:));
        xlabel('t');
        ylabel('p');

% End of file %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%