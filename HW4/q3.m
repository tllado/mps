% HW4_3a.m
% 
% A simple numeric solver
% 
% Created by Travis Llado, travisllado@utexas.edu, 2018-10-30

a = 1e10;
b = 1000;
k = 0.1;
m = 1;

F0 = -10:0.1:10;

xc = zeros(1,length(F0));
options = optimset('Display','off');

bar = waitbar(0, 'Solving ...');
for ii = 1:length(F0)
    waitbar(ii/length(F0),bar);
    
    F = @(x) x+a*x^3-F0(ii)/k;
    xc(ii) = fsolve(F, rand,options);
end
close(bar);

plot(xc,F0);
xlabel('F_0');
ylabel('x_e_q');
