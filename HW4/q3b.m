% HW4_3a.m
% 
% A simple numeric solver
% 
% Created by Travis Llado, travisllado@utexas.edu, 2018-10-30

a = 1e10;
b = 1000;
k = 0.1;
m = 1;

Fi = -10:0.1:10;

xc = zeros(1,length(Fi));
options = optimset('Display','off');

bar = waitbar(0, 'Solving ...');
for ii = 1:length(Fi)
    waitbar(ii/length(Fi),bar);
    
    F = @(x) x+a*x^3-Fi(ii)/k;
    xc(ii) = fsolve(F, rand,options);
end
close(bar);

plot(Fi,xc);
xlabel('F_0');
ylabel('x_e_q');
