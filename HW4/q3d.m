b = 1000;
m = 1;

p = -1e-4:1e-7:1e-4;
B = -b*abs(p).*p/m^2;

bl = (p(1)-p(length(p)))/(B(1)-B(length(B)));

plot(p,B, [p(1) p(length(p))],[B(1) B(length(B))]);
xlabel('p');
legend('original -b|p|p/m^2','linearized b_l_i_np');
title(['b_lin=' num2str(bl)]);