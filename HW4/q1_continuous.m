% HW4_1.m
% 
% A dynamics simulation for demonstrating hysteresis
% 
% Created by Travis Llado, travisllado@utexas.edu, 2018-10-30

% User variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
b = 1;    % Stiction Threshold
k = 1;      % Spring Constant

A = [ -k/b ];  % System Model
B = [ 1 ];  % Input Model

dt = 0.0001;  % Timestep Length
tF = 10;    % Simulation Length

% Program variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t = 0:dt:tF;                % Simulation Time
x = zeros(1, length(t));    % State
f = cos(t);                 % Flow Input
q = sin(t);                 % Input Displacement (integral of Flow)
e = zeros(1, length(t));    % Required Effort Input

% Simulation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for ii = 2:length(t)
    dx = A*x(ii-1) + B*f(ii);
    x(ii) = x(ii-1) + dx*dt;
    e(ii) = k*x(ii);
end

% Plot results %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
    subplot(2,2,1)
        plot(t,f, t,e);
        xlabel('Time');
        ylabel('Inputs');
        legend('Flow','Effort')
    subplot(2,2,2)
        plot(t,x);
        xlabel('Time');
        ylabel('Spring Displacement');
    subplot(2,2,3)
        plot(q,x);
        xlabel('Input Displacement');
        ylabel('Spring Displacement');

% End of file %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%