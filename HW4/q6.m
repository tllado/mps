% Double Mass-Spring-Damper Simulation

% User variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
b1 = 30;
b2 = 10;
k1 = 500;
k2 = 100;
m1 = 19;
m2 = 20;
g = -9.81;

    %   x1  x2  p1          p2
A = [   0   0   -1/m1       0       ;   % dx1
        0   0   1/m1        -1/m2   ;   % dx2
        k1  -k2 (-b1-b2)/m1 b2/m2   ;   % dp1
        0   k2  b2/m1       -b2/m2  ];  % dp2  
    %   s   g
B = [ 	1   0   ;   % dx1
        0   0   ;   % dx2
        b1  m1  ;   % dp1
        0   m2  ];  % dp2

v = @(x) sin(10*x);

dt = 0.0001;
tF = 10;

% Program variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t = 0:dt:tF;
x = zeros(length(A), length(t));
u = [   v(t)                    ;
        repmat(g, 1,length(t))  ];

% Simulation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for ii = 2:length(t)
    dx = A*x(:,ii-1) + B*u(:,ii);
    x(:,ii) = x(:,ii-1) + dx*dt;
end

% Plot results %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
    subplot(2,2,1)
        plot(t,x(1,:));
        xlabel('t');
        ylabel('x1');
    subplot(2,2,2)
        plot(t,x(2,:));
        xlabel('t');
        ylabel('x2');
    subplot(2,2,3)
        plot(t,x(3,:));
        xlabel('t');
        ylabel('p1');
    subplot(2,2,4)
        plot(t,x(4,:));
        xlabel('t');
        ylabel('p2');

% End of file %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%