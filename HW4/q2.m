% HW4_1.m
% 
% A dynamics simulation for demonstrating hysteresis
% 
% Created by Travis Llado, travisllado@utexas.edu, 2018-10-30

% User variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bt1 = 1e3;  % guess
bt2 = 1e3;  % guess
bs1 = 0.75e3;
bs2 = 0.75e3;
kt1 = 200e3;
kt2 = 200e3;
ks1 = 30e3;
ks2 = 20e3;
mt1 = 100;  % guess
mt2 = 100;  % guess
mv  = 1700 - mt1 - mt2;
J   = 2704;
l1  = 1.3;  % guess
l2  = 1.4;  % guess
g   = -9.81;

    %   xt1 xt2 pt1             pt2             xs1     xs2     pv                  h
A = [   0   0   -1/mt1          0               0       0       0                   0                   ;   % xd_t1
        0   0   0               -1/mt2          0       0       0                   0                   ;   % xd_t2
        kt1 0   -(bt1+bs1)/mt1  0               -ks1    0       -bs1/mv             bs1/l1/J            ;   % pd_t1
        0   kt2 0               -(bt2+bs2)/mt2  0       -ks2    -bs2/mv             -bs2/l2/J           ;   % pd_t2
        0   0   1/mt1           0               0       0       -1/mv               -1/l1/J             ;   % xd_s1
        0   0   0               1/mt2           0       0       -1/mv               -1/l2/J             ;   % xd_s2
        0   0   bs1/mt1         bs2/mt2         ks1     ks2     -(bs1+bs2)/mv       (bs1/l1-bs2/l2)/J   ;   % pd_v
        0   0   -l1*bs1/mt1     l2*bs2/mt2      -l1*ks1 l2*ks2  (l1*bs1-l2*bs2)/mv  (bs1-bs2)/J         ];  % hd

    %   v1  v2  g
B = [   1   0   0   ;   % xd_t1
        0   1   0   ;   % xd_t2
        bt1 0   mt1 ;   % pd_t1
        0   bt2 mt2 ;   % pd_t2
        0   0   0   ;   % xd_s1
        0   0   0   ;   % xd_s2
        0   0   mv  ;   % pd_v
        0   0   0   ];  % hd

dt = 0.00001; % Timestep Length
tF = 5;    % Simulation Length

v1 = 0;
v2 = 0;
u = repmat([v1;	v2; g], 1,length(0:dt:tF));

% Program variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t = 0:dt:tF;    % Simulation Time
x = zeros(length(A), length(t));

% Simulation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bar = waitbar(0, 'Running Simulation');
for ii = 2:length(t)
    if mod(ii,100) == 0
        waitbar(ii/length(t),bar);
    end
    
    dx = A*x(:,ii-1) + B*u(:,ii);
    x(:,ii) = x(:,ii-1) + dx*dt;
end

% Plot results %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
waitbar(1,bar,'Building plots');
figure
    subplot(2,4,1)
        plot(t,x(1,:));
        ylabel('x_t_1');
    subplot(2,4,2)
        plot(t,x(3,:));
        ylabel('p_t_1');
    subplot(2,4,3)
        plot(t,x(5,:));
        ylabel('x_s_1');
    subplot(2,4,4)
        plot(t,x(7,:));
        ylabel('p_v');
    subplot(2,4,5)
        plot(t,x(2,:));
        ylabel('x_t_2');
    subplot(2,4,6)
        plot(t,x(4,:));
        ylabel('p_t_2');
    subplot(2,4,7)
        plot(t,x(6,:));
        ylabel('x_s_2');
    subplot(2,4,8)
        plot(t,x(8,:));
        ylabel('h');
close(bar);

% End of file %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%