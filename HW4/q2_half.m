% HW4_1.m
% 
% A dynamics simulation for demonstrating hysteresis
% 
% Created by Travis Llado, travisllado@utexas.edu, 2018-10-30

% User variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bt = 1e3;
bs = 0.75e3;
kt = 200e3;
ks = 30e3;
mt = 100;
mv = 1700;
g = -9.81;

    %   xt  pt          xs  pv
A = [   0   -1/mt       0   0       ;   % xd_t
        kt  -(bt+bs)/mt -ks bs/mv   ;   % pd_t
        0   1/mt        0   -1/mv   ;   % xd_s
        0   bs/mt       ks  -bs/mv  ];  % pd_v

    %   v   g
B = [   1   0   ;   % xd_t
        bt  mt  ;   % pd_t
        0   0   ;   % xd_s
        0   mv  ];  % pd_v

dt = 0.000001; % Timestep Length
tF = 1;    % Simulation Length

v = 0;
u = repmat([v; g], 1,length(0:dt:tF));

% Program variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t = 0:dt:tF;    % Simulation Time
x = zeros(length(A), length(t));

% Simulation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for ii = 2:length(t)
    dx = A*x(:,ii-1) + B*u(:,ii);
    x(:,ii) = x(:,ii-1) + dx*dt;
end

% Plot results %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
    subplot(2,2,1)
        plot(t,x(1,:));
        ylabel('xt');
    subplot(2,2,2)
        plot(t,x(2,:));
        ylabel('pt');
    subplot(2,2,3)
        plot(t,x(3,:));
        ylabel('xs');
    subplot(2,2,4)
        plot(t,x(4,:));
        ylabel('pv');

% End of file %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%