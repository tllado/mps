% HW4_3c.m
% 
% A nonlinear dynamics simulation
% 
% Created by Travis Llado, travisllado@utexas.edu, 2018-10-31

% User variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
a = 1e10;
b = 1000;
k = 0.1;
m = 1;
Fi = 1;

dt = 0.00001;   % Timestep Length
tF = 10;        % Simulation Length

% Program variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t = 0:dt:tF;
x = zeros(2,length(t));
options = optimset('Display','off');

F = @(x) x+a*x^3-Fi/k;
x(1,1) = fsolve(F, rand,options);

% Simulation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bar = waitbar(0, 'Running Simulation');
for ii = 2:length(t)
    if mod(ii,100) == 0
        waitbar(ii/length(t),bar);
    end
    
    dx = [  x(2,ii-1)/m  ;
            -10*x(2,ii-1)-k*(x(1,ii-1)+a*x(1,ii-1)^3)+Fi    ];
    x(:,ii) = x(:,ii-1) + dx*dt;
end

% Plot results %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
waitbar(1,bar,'Building plots');
figure
    subplot(1,2,1)
        plot(t,x(1,:));
        xlabel('t');
        ylabel('x');
    subplot(1,2,2)
        plot(t,x(2,:));
        xlabel('t');
        ylabel('p');
        title('\DeltaF = 0');
close(bar);

% End of file %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%