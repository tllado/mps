% Double Mass-Spring-Damper Simulation

% User variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bt1 = 1e3;
bt2 = 1e3;
bs1 = 0.75e3;
bs2 = 0.75e3;
kt1 = 200e3;
kt2 = 200e3;
ks1 = 30e3;
ks2 = 20e3;
mt1 = 100;
mt2 = 100;
mv  = 1700-mt1-mt2;
J   = 2704;
l1  = 1.109;
l2  = 1.591;
g   = -9.81;
h   = 0.55;

    %   xt1 xt2 pt1             pt2             xs1     xs2     pv                  h
A = [   0   0   -1/mt1          0               0       0       0                   0                       ;   % dxt1
        0   0   0               -1/mt2          0       0       0                   0                       ;   % dxt2
        kt1 0   (-bt1-bs1)/mt1  0               -ks1    0       bs1/mv              -bs1*l1/J               ;   % dpt1
        0   kt2 0               (-bt2-bs2)/mt2  0       -ks2    bs2/mv              bs2*l2/J                ;   % dpt2
        0   0   1/mt1           0               0       0       -1/mv               l1/J                    ;   % dxs1
        0   0   0               1/mt2           0       0       -1/mv               -l2/J                   ;   % dxs2
        0   0   bs1/mt1         bs2/mt2         ks1     ks2     (-bs1-bs2)/mv       (bs1*l1-bs2*l2)/J       ;   % dpv
        0   0   -l1*bs1/mt1     l2*bs2/mt2      -l1*ks1 l2*ks2  (l1*bs1-l2*bs2)/mv  (-l1^2*bs1-l2^2*bs2)/J  ];  % dh
        
    %   v1  v2  g   h
B = [   1   0   0   0   ;   % dxt1
        0   1   0   0   ;   % dxt2
        bt1 0   mt1 0   ;   % dpt1
        0   bt2 mt2 0   ;   % dpt2
        0   0   0   0   ;   % dxs1
        0   0   0   0   ;   % dxs2
        0   0   mv  0   ;   % dpv
        0   0   0   0   ];  % dh

    %   xt1         xt2         pt1 pt2 xs1         xs2         pv  h
C = [   1/(l1+l2)   -1/(l1+l2)  0   0   1/(l1+l2)   -1/(l1+l2)  0   0   ;   % th
        -1          0           0   0   -1          0           0   0   ;   % hf
        0           -1          0   0   0           -1          0   0   ];  % hr

    %   v1  v2  g   h
D = [   0   0   0   0   ;   % th
        0   0   0   1   ;   % hf
        0   0   0   1   ];  % hr

v1 = @(x) 0.1*sin(19*2*pi*x);
v2 = @(x) 0.1*sin(23*2*pi*x);

dt = 0.0001;
tF = 20;

% Program variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t = 0:dt:tF;
x = zeros(length(A), length(t));
u = [   v1(t)                   ;
        v2(t)                   ;
        repmat(g, 1,length(t))  ;
        repmat(h, 1,length(t))  ];
y = zeros(size(C,1), length(t));

% Simulation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bar = waitbar(0, 'Running Simulation');
for ii = 2:length(t)
    if mod(ii,round(length(t)/100)) == 0
        waitbar(ii/length(t),bar);
    end
    
    dx = A*x(:,ii-1) + B*u(:,ii);
    x(:,ii) = x(:,ii-1) + dx*dt;
    y(:,ii) = C*x(:,ii) + D*u(:,ii);
end

% Plot results %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
waitbar(1,bar,'Building plots');
figure
    subplot(3,4,1)
        plot(t,x(1,:));
        xlabel('t (s)');
        ylabel('x_{tire,front} (m)');
    subplot(3,4,2)
        plot(t,x(3,:));
        xlabel('t (s)');
        ylabel('p_{tire,front} (kg-m/s)');
    subplot(3,4,3)
        plot(t,x(5,:));
        xlabel('t (s)');
        ylabel('x_{spring,front} (m)');
    subplot(3,4,4)
        plot(t,x(7,:));
        xlabel('t (s)');
        ylabel('p_{vehicle} (kg-m/s)');
    subplot(3,4,5)
        plot(t,x(2,:));
        xlabel('t (s)');
        ylabel('x_{tire,rear} (m)');
    subplot(3,4,6)
        plot(t,x(4,:));
        xlabel('t (s)');
        ylabel('p_{tire,rear} (kg-m/s)');
    subplot(3,4,7)
        plot(t,x(6,:));
        xlabel('t (s)');
        ylabel('x_{spring,rear} (m)');
    subplot(3,4,8)
        plot(t,x(8,:));
        xlabel('t (s)');
        ylabel('h (kg-m^2/s)');
    subplot(3,4,9)
        plot(t,y(1,:), [0 20],[0 0], '--');
        xlabel('t (s)');
        ylabel('\theta (rad)');
    subplot(3,4,10)
        plot(t,y(2,:));
        xlabel('t (s)');
        ylabel('height_{front} (m)');
    subplot(3,4,11)
        plot(t,y(3,:));
        xlabel('t (s)');
        ylabel('height_{rear} (m)');
close(bar);

% End of file %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%