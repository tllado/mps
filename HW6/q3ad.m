% q3ad.m
% A dynamics simulation for a governor subsystem
% Created by Travis Llado, travisllado@utexas.edu, 2018-11-26

clear all

% User variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
l = 0.152;      % link length (m)

m1 = 1;
r1 = 0.3;
j1 = m1*r1^2/2;

m2 = 1;
r2 = 0.152;
j2 = m2*r2^2/2;
k2 = 500;

b3 = 10;        % linear rotational damping of roller 3 (N-m-sec/rad)
m3 = 1;         % mass of roller 3 (kg)
r3 = 0.076;     % radius of roller 3
j3 = m3*r3^2/2; % mass moment of inertia(kg-m^2)

k4 = 250;       % governor spring stiffness (N/m)
m4 = 0.5;       % governor flyball mass (kg)

b5 = 10;        % linear damping of slider (N-sec/m)
m5 = 0.1;       % slider mass (kg)

k24 = k2+k4;
m25 = m2+m5;

t_in = 1; % input torque, N-m
dt = 0.001; % Timestep Length
tF = 100;    % Simulation Length

% Program variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t = 0:dt:tF;    % Simulation Time
x = zeros(3,length(t));    % [h p x]
x(3,1) = 0.01;
T = repmat(t_in, 1,length(t));
T(round(length(t)*1/5)+1:round(length(t)*2/5)) = T(1)*1.25;
T(round(length(t)*2/5)+1:round(length(t)*3/5)) = T(1);
T(round(length(t)*3/5)+1:round(length(t)*4/5)) = T(1)*1.5;
T(round(length(t)*4/5)+1:round(length(t)*5/5)) = T(1);
th = zeros(1,length(t));
Fc = th;

% Simulation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bar = waitbar(0, 'Running Simulation');
for ii = 2:length(t)
    if mod(ii,round(length(t)/100)) == 0
        waitbar(ii/length(t),bar);
    end
    
    if x(3,ii-1) <= 0.1
        x(3,ii-1) = 0.1;
    elseif x(3,ii-1) >= 2*l*0.9
        x(3,ii-1) = 2*l*0.9;
    end
    th(ii) = acos((2*l-x(3,ii-1))/2/l);
    
    n21 = r2/(r1-x(3,ii-1));
    n32 = r3/r2;
    r4 = l*sin(th(ii));
    Fc(ii) = 2*m4*r4*(x(1,ii-1)/j1)^2;
    j4 = 2*m4*r4^2;
    tn = tan(th(ii));
    
    a1 = 1 + n21^2*j2/j1 + n21^2*n32^2*j3/j1 + n21^2*n32^2*j4/j1 ;
    a2 = 1 + 1/tn^2*m4/m25 + 1/4*m4/m25;
            
        %   h                       p           x
    A = [   (-n21^2*n32^2*b3/j1)    0           0       ;   % h_d
            0                       (-b5/m25)   (-k24)  ;   % p_d
            0                       (1/m25)     0       ];  % x_d
        %   T   Fc
    B = [   1   0       ;   % h_d
            0   1/tn    ;   % p_d
            0   0       ];  % x_d
    u = [   T(ii);  Fc(ii)  ];
            
    dx = (A*x(:,ii-1) + B*u).*[1/a1; 1/a2; 1];
    x(:,ii) = x(:,ii-1) + dx*dt;
end

% Plot results %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
waitbar(1,bar,'Building plots');
figure
    subplot(5,1,1);
        plot(t,T);
        xlabel('t (sec)');
        ylabel('\tau_{in} (N-m)');
    subplot(5,1,2);
        plot(t,x(1,:));
        xlabel('t (sec)');
        ylabel('h');
    subplot(5,1,3);
        plot(t,x(2,:));
        xlabel('t (sec)');
        ylabel('p');
    subplot(5,1,4);
        plot(t,x(3,:));
        xlabel('t (sec)');
        ylabel('x (m)');
%     subplot(5,1,5);
%         plot(t,th);
%         xlabel('t (sec)');
%         ylabel('\theta (rad)');
    subplot(5,1,5)
        plot(t,Fc);
        ylabel('Fc');
close(bar);

% End of file %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%