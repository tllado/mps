% q3ac.m
% A dynamics simulation for a governor subsystem
% Created by Travis Llado, travisllado@utexas.edu, 2018-11-26

clear all

% User variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
l = 0.152;      % link length (m)
b3 = 10;        % linear rotational damping of roller 3 (N-m-sec/rad)
m3 = 1;         % mass of roller 3 (kg)
r3 = 0.076;     % radius of roller 3
j3 = m3*r3^2/2; % mass moment of inertia(kg-m^2)
k4 = 250;       % governor spring stiffness (N/m)
m4 = 0.5;       % governor flyball mass (kg)
b5 = 10;        % linear damping of slider (N-sec/m)
m5 = 0.1;       % slider mass (kg)

dt = 0.001; % Timestep Length
tF = 10;    % Simulation Length

% Program variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t = 0:dt:tF;    % Simulation Time
x = zeros(3, length(t));    % [h p x]
x(3,1) = 0.01;
F = repmat(1.3132, 1,length(t));
T = repmat(200/3*pi, 1,length(t));
T(round(length(t)*1/5)+1:round(length(t)*2/5)) = T(1)*1.25;
T(round(length(t)*2/5)+1:round(length(t)*3/5)) = T(1);
T(round(length(t)*3/5)+1:round(length(t)*4/5)) = T(1)*1.5;
T(round(length(t)*4/5)+1:round(length(t)*5/5)) = T(1);

% Simulation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bar = waitbar(0, 'Running Simulation');
for ii = 2:length(t)
    if mod(ii,round(length(t)/100)) == 0
        waitbar(ii/length(t),bar);
    end
    
    th = acos((l-x(3,ii-1))/l);
    a1 = 1 + 2*m4*(l*sin(th))^2/j3;
    a2 = 1 + m4/4/m3 + m4/(tan(th))^2/m3;
    
    AxBu = [    -b5/j3/a1*x(1,ii-1)+1/a1*T(ii)  ;
                -b3/a2/m3*x(2,ii-1)-k4/a2*x(3,ii-1)+1/a2*F(ii) ...
                    + 1/a2/tan(th)*m4*(x(1,ii-1)/j3)^2*l*sin(th)    ;
                1/m3*x(2,ii-1)  ];
            
    dx = AxBu;
    x(:,ii) = x(:,ii-1) + dx*dt;
end

% Plot results %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
waitbar(1,bar,'Building plots');
figure
    subplot(3,1,1);
        plot(t,T);
        xlabel('t (sec)');
        ylabel('\tau_{in} (N-m)');
    subplot(3,1,2);
        plot(t,x(3,:));
        xlabel('t (sec)');
        ylabel('x (m)');
    subplot(3,1,3);
        plot(t,acos((l-x(3,:))/l));
        xlabel('t (sec)');
        ylabel('\theta (rad)');
close(bar);

% End of file %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%