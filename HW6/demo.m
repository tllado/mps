% test.m
% A dynamics simulation for a very simple governor valve
% Created by Travis Llado, travisllado@utexas.edu, 2018-11-15

% User variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
m1 = 1;     % total fluid momentum
a1 = 20;    % throttle valve scalar
a2 = 1;     % throttle valve offset
b1 = NaN;   % throttle valve damping, f(a1,a2)
a3 = 1;     % abstract conversion from fluid pressure to propeller torque
m2 = 1;     % total moment of inertia
b2 = 2;     % rotational damping

B = [   1   0   ;
        0   1   ];  % Input model

dt = 0.001; % Timestep Length
tF = 30;    % Simulation Length

% input fluid pressure
F = zeros(1,length(0:dt:tF));
F(round(length(F)*1/6+1):round(length(F)*2/6)) = 1;
F(round(length(F)*2/6+1):round(length(F)*3/6)) = 2;
F(round(length(F)*3/6+1):round(length(F)*4/6)) = 3;
F(round(length(F)*4/6+1):round(length(F)*5/6)) = 4;
F(round(length(F)*5/6+1):round(length(F)*6/6)) = 2;

% Program variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t = 0:dt:tF;    % Simulation Time
x = zeros(2, length(t));

% Simulation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bar = waitbar(0, 'Running Simulation');
for ii = 2:length(t)
    if mod(ii,round(length(t)/100)) == 0
        waitbar(ii/length(t),bar);
    end
    
    if x(2,ii-1) > a2
        b1 = a1*(x(2,ii-1)-a2);
    else
        b1 = 0;
    end
    A = [   -b1/m1  0       ;
            0       -b2/m2  ];
    
    U = [   F(ii);  x(1,ii-1)   ];
            
    dx = A*x(:,ii-1) + B*U;
    x(:,ii) = x(:,ii-1) + dx*dt;
end

% Plot results %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
waitbar(1,bar,'Building plots');
figure
    subplot(3,1,1);
        plot(t,F);
        xlabel('t');
        ylabel('input pressure');
        title('b_1(x) = a_1x+a_2');
    subplot(3,1,2);
        plot(t,x(1,:));
        xlabel('t');
        ylabel('fluid momentum');
    subplot(3,1,3);
        plot(t,x(2,:));
        xlabel('t');
        ylabel('angular momentum');
close(bar);

% End of file %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%