% HW2B_6.m
% 
% A dynamics simulation in which a small boat is towed by a larger ship
% with a cable that has finite stiffness
% 
% Created by Travis Llado, travisllado@utexas.edu, 2018-09-22

% User variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
k = 36485;  % Cable stiffness   (N/m)       = 1000/0.40 lbm/ft
m = 14606;  % Mass              (kg)        = 32200 lbm
b = 51079;  % Coef Friction     (N-sec/m)   = 3500 lbf-sec/ft
V = 1.524;  % Speed             (m/sec)     = 5 ft/sec

A = [   0   -1      ;   % System Model
        k/m -b/m    ];
B = [1; 0];             % Input Model

x_k_0 = 0;  % Initial line tension  N
xd_2_0 = 0; % Initial boat speed    m/sec

dt = 0.01;  % Timestep length   sec
tF = 10;    % Total runtime     sec

% Program variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t = 0:dt:tF;    % Simulation Time   sec

x = zeros(2, length(t));    % x(i) = [x_k; xdot_2];
x(:,1) = [x_k_0; xd_2_0];   % Set initial values

u = V;  % Set constant input

% Simulation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bar = waitbar(0, 'Running Simulation');
for ii = 2:length(t)
    waitbar(ii/length(t),bar);
    
    % System
    dx = A*x(:,ii-1) + B*u;
    x(:,ii) = x(:,ii-1) + dx*dt;
end

% Plot results %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
waitbar(1,bar,'Building plots');
figure
    subplot(2,1,1)
        plot(t,x(1,:));
        xlabel('time (sec)');
        ylabel('cable extension (m)');
    subplot(2,1,2)
        plot([0,tF],[V,V], t,x(2,:));
        xlabel('time (sec)');
        ylabel('boat speeds (m/sec)');
        legend('lead boat', 'towed boat');
close(bar);

% End of file %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%