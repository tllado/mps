# HW2B_6.py
# 
# A dynamics simulation in which a small boat is towed by a larger ship
# with a cable that has finite stiffness
# 
# Created by Travis Llado, travisllado@utexas.edu, 2018-09-22

import numpy as np
import matplotlib.pyplot as plt

# User variables ###############################################################
k = 36485   # Cable stiffness   (N/m)       = 1000/0.40 lbm/ft
m = 14606   # Mass              (kg)        = 32200 lbm
b = 51079   # Coef Friction     (N-sec/m)   = 3500 lbf-sec/ft
V = 1.524   # Speed             (m/sec)     = 5 ft/sec

A = np.array([[ 0,      -1      ],
              [ k/m,    -b/m    ]]) # System Model
B = np.array([  1,  0   ])          # Input Model

x_k_0 = 0   # Initial line tension  N
xd_2_0 = 0  # Initial boat speed    m/sec

dt = 0.01   # Timestep length   sec
tF = 10     # Total runtime     sec

# Program variables ############################################################
t = np.arange(0, tF+dt, dt) # Simulation Time   sec

x = np.zeros((2, len(t)))   # x(:,i) = [x_k, xdot_2]
x[:,0] = [x_k_0, xd_2_0]    # Set initial values

u = V   # Set constant input

# Simulation ###################################################################
for ii in range(1, len(t)):
    dx = A.dot(x[:,ii-1]) + B.dot(u)
    x[:,ii] = x[:,ii-1] + dx*dt

# Plot results #################################################################
plt.figure(1)

plt.subplot(121)
plt.plot(t, x[0])
plt.xlabel('time (sec)')
plt.ylabel('cable extension (m)')

plt.subplot(122)
plt.plot(t, x[1], label='towed boat')
plt.plot([0,tF], [V,V], label='lead boat')
plt.xlabel('time (sec)')
plt.ylabel('boat speeds (m/sec)')
plt.legend(loc='lower right')
plt.show()

# End of file ##################################################################