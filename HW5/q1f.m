% q1f.m
% 
% A dynamics simulation for a "bucking bronco"
% 
% Created by Travis Llado, travisllado@utexas.edu, 2018-11-12

% User variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
wg = 10;    % rad/sec
L = 0.01;   % Henrys
R = 0.1;    % Ohms
rm = 1;     % N-m/A
J = 10;     % kg-m^2
b = 1;      % N-m-sec/rad
a = 0.010099;      % ?
h0 = 0.1;   % rad/sec

    %   lambda  h
A = [   -R/L    rm/L    ;   % lambda_dot
        -rm/J   -b/J    ];  % h_dot

dt = 0.0001; % Timestep Length
tF = 30;    % Simulation Length

u = wg; % input

% Program variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t = 0:dt:tF;    % Simulation Time
x = zeros(length(A), length(t));
u = repmat(u, 1, length(t));

x(2,1) = h0;    % initial value

% Simulation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bar = waitbar(0, 'Running Simulation');
for ii = 2:length(t)
    if mod(ii,100) == 0
        waitbar(ii/length(t),bar);
    end
    
    rg = a*x(1,ii-1)/L;
        %   wg
    B = [   rg  ;   % lambda_dot
            0   ];  % h_dot
    dx = A*x(:,ii-1) + B*u(:,ii);
    x(:,ii) = x(:,ii-1) + dx*dt;
end

% Plot results %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
waitbar(1,bar,'Building plots');
figure
    subplot(1,2,1)
        plot(t,x(1,:));
        xlabel('time (sec)');
        ylabel('\lambda (kg-m^2/s^2-A)');
    subplot(1,2,2)
        plot(t,x(2,:));
        xlabel('time (sec)');
        ylabel('h (kg-m^2/sec)');
close(bar);

% End of file %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%