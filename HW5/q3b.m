% q3b.m
% 
% A dynamics simulation for a rotating actuator
% 
% Created by Travis Llado, travisllado@utexas.edu, 2018-11-14

% User variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
K = 1.0;    % N-m/rad
B = 0.01;   % N-sec/rad
J = 0.001;  % kg-m^2
a = 1;      % Henrys
b = -2/pi;  % Henrys
R = 100;    % Ohms
offset = deg2rad(45);
L = @(th) a + b*abs(th + offset);  % Henrys

A = 10; % V
f = 100;  % rad/sec
v = @(t) A*sin(f*t);    % input voltage

dt = 0.001;    % Timestep Length
tF = 3;        % Simulation Length

% Program variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t = 0:dt:tF;    % Simulation Time
x = zeros(3, length(t));
v = v(t);

% Simulation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bar = waitbar(0, 'Running Simulation');
for ii = 2:length(t)
    if mod(ii,round(length(t)/100)) == 0
        waitbar(ii/length(t),bar);
    end
    
    lam = x(1,ii-1);
    th =  x(2,ii-1);
    h =   x(3,ii-1);
    
    dx = [  v(ii) - R/L(th)*lam                 ;
            h/J                                 ;
            b*lam^2/2/L(th)^2 - B/J*h - K*th    ];
    x(:,ii) = x(:,ii-1) + dx*dt;
end

% Plot results %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
waitbar(1,bar,'Building plots');
figure
    subplot(3,1,1)
        plot(t,x(1,:));
        xlabel('time (sec)');
        ylabel('\lambda (kg-m^2/s^2-A)');
        title(['Using v_{in} = ' num2str(A) ' sin(' num2str(f) ...
            't) and \theta_{offset} = ' num2str(rad2deg(offset)) char(176)]);
    subplot(3,1,2)
        plot(t,x(2,:));
        xlabel('time (sec)');
        ylabel('\theta (rad)');
    subplot(3,1,3)
        plot(t,x(3,:));
        xlabel('time (sec)');
        ylabel('h (kg-m^2/sec)');
close(bar);

% End of file %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%