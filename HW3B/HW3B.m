% HW3B.m
% 
% A dynamics simulation of a single actuator powering an excavator arm
% 
% Created by Travis Llado, travisllado@utexas.edu, 2018-10-16

% User variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
R_m = 0.408;    % Motor Resistance  (ohms)
L = 1.710e-3;   % Motor Inductance  (Henrys)
r = 0.511;      % Motor Constant    (sec^-1/V)

J_a = 4939e-7;  % Actuator Mom.Inertia  (kg-m^2)
b_a = 0.1;      % Actuator Friction     (N/(m/s))
n = 5/100/2/pi; % Actuator Reduction    (m/rad)

l_DB = 0.33;    % Arm Length 1      (m)
l_BC = 1.33;    % Arm Length 2      (m)
k = 1000;       % Spring Constant   (N)
J_d = 3.58;     % Arm Mom.Inertia   (m-N^2)
b_b = 189;      % Soil Friction     (N/(m/s))

% Initial State
x_0 = [ 0   ;   % I_m
        0   ;   % w_a
        0   ;   % w_d
        0   ];  % th_k

% System Model
A = [   -R_m/L  -r/L    0           0               ;
        r/J_a   0       0           -n*l_DB*k/J_a   ;
        0       0       -b_b/J_d    k/J_a           ;
        0       n*l_DB  -1          0               ];

% Input Model
B = [   1/L ;
        0   ;
        0   ;
        0   ];

u = @(t) 28*sin(t/2);   % Input Voltage

dt = 0.00001;   % Timestep length   (sec)
tF = 10;         % Total runtime     (sec)

% Program variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t = 0:dt:tF;    % Simulation Time   (sec)

x = zeros(length(A), length(t));
x(:,1) = x_0;   % Set initial values

u = u(t);

y = zeros(1, length(t));

% Simulation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bar = waitbar(0, 'Running Simulation');
for ii = 2:length(t)
    waitbar(ii/length(t),bar);
    
    % System
    dx = A*x(:,ii-1) + B*u(ii);
    x(:,ii) = x(:,ii-1) + dx*dt;
    y(ii) = y(ii-1) + x(3,ii)*dt;
end

% Plot results %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
waitbar(1,bar,'Building plots');
figure
    subplot(3,2,1)
        plot(t,u);
        xlabel('time (sec)');
        ylabel('V_s (V)');
    subplot(3,2,2)
        plot(t,y);
        xlabel('time (sec)');
        ylabel('\theta_d (rad)');
    subplot(3,2,3)
        plot(t,x(1,:));
        xlabel('time (sec)');
        ylabel('I_m (A)');
    subplot(3,2,4)
        plot(t,x(2,:));
        xlabel('time (sec)');
        ylabel('\omega_a (rad/sec)');
    subplot(3,2,5)
        plot(t,x(3,:));
        xlabel('time (sec)');
        ylabel('\omega_d (rad/sec)');
    subplot(3,2,6)
        plot(t,x(4,:));
        xlabel('time (sec)');
        ylabel('\theta_k (rad)');
close(bar);

% End of file %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%